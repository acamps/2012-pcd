/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_sessio1;

import java.util.Stack;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack {
    private Stack gStack;
    private int MAXELEM;
    private int count;

    public PCD_Gstack(int MAXELEM) {
        this.gStack = new Stack();
        this.MAXELEM = MAXELEM;
        this.count = 0;
    }
    
    public synchronized void PCD_Gstack_push(Object object) {
        boolean reachedMax = ((this.count + 1) > this.MAXELEM);
        if(!reachedMax) {
            Object tmp = this.gStack.push(object);
            this.count++;
            System.out.println("PUSH: " + (Integer)tmp + "  STACK COUNT: " + this.count);
        } else {
            System.out.println("REACHED STACK MAX SIZE " + this.count + " " + reachedMax);
        }
    }
    
    public synchronized Object PCD_Gstack_pop() {
        boolean enoughElements = (this.count > 0);
        if(enoughElements) {
            Object tmp = gStack.pop();
            this.count--;
            System.out.println("POP: " + (Integer)tmp + "  STACK COUNT: " + this.count);
            return tmp;
        }
        System.out.println("NOT ENOUGH ELEMENTS");
        return null;
    }
    
    
    
}
