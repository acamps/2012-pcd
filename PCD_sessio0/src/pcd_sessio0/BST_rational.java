/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_sessio0;

import com.sun.media.jai.util.Rational;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author albertcamps
 */
public class BST_rational extends BST<Rational> {

    public BST_rational(Rational a) {
        this.setValue(a);
    }
    public BST_rational() {}

    public void insert() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //int n = s.nextInt();
        char c;
        boolean num = true;
        boolean firstInsert = true;
        long tmpNum = 0, tmpDenom = 1;
        do {
            c = (char) br.read();
            //System.out.println(c);
            if(c == ' ' || c == '\n'){
                //System.out.println("First rational found!!: " + tmpNum + " " + tmpDenom);
                if(firstInsert) {
                    this.setValue(new Rational(tmpNum, tmpDenom));
                    firstInsert = false;
                } else {
                    insert(new BST_rational(new Rational(tmpNum, tmpDenom)), this);
                }
                tmpNum = 0;
                tmpDenom = 1;
                num = true;
            } else if (c == '/') {
                num = false;
                tmpDenom = 0;
            } else if (num){
                long tmpLong = Long.parseLong(String.valueOf(c));
                tmpNum *= 10;
                tmpNum += tmpLong;
            } else {
                long tmpLong = Long.parseLong(String.valueOf(c));
                tmpDenom *= 10;
                tmpDenom += tmpLong;
            }
        } while(c != '\n');
    }
    
    @Override
    public int compare(Rational a, Rational b) {
        int result = 0;
        System.out.println("Comparo " + a.num+ "/"+  a.denom + " con "+ b.num+ "/"+  b.denom);
        long tmpNum = (a.num*b.denom) - (b.num*a.denom);
        if(tmpNum > 0) {
            result = 1;
        } else if(tmpNum < 0){
            result = -1;
        }
        return result;
    }

    @Override
    public String print(BST<Rational> a) {
        Rational tmpRational = a.getValue();
        String tmp = String.valueOf(tmpRational.num)+"/"+String.valueOf(tmpRational.denom);
        return tmp;
    }
    
}
