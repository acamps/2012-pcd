package pcd_sessio1;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack_producer extends Thread{
    private PCD_Gstack pila;

    public PCD_Gstack_producer(PCD_Gstack pila) {
        this.pila = pila;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            this.pila.PCD_Gstack_push((Object)i);
        }
    }

}
