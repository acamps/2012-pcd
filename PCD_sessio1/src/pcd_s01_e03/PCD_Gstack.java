/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_s01_e03;

import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack {
    private Stack gStack;
    private int MAXELEM;
    private int count;

    public PCD_Gstack(int MAXELEM) {
        this.gStack = new Stack();
        this.MAXELEM = MAXELEM;
        this.count = 0;
    }
    
    public int getCount() {
        return this.count;
    }
    
    private boolean reachedMax(int amount) {
        return ((this.count + amount) > this.MAXELEM);
    }
    
    public boolean enoughElements(int amountToInsert) {
        return(this.count >= amountToInsert);
    }
    
    public synchronized void PCD_Gstack_push(Object object) {
        
        while(reachedMax(1)) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(PCD_Gstack.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Object tmp = this.gStack.push(object);
        
        this.count += 1;
        this.notifyAll();
        System.out.println("PUSH: " + (Integer)object + "  STACK COUNT: " + this.count);
    }
    
    public synchronized Object PCD_Gstack_pop() {
        while(!enoughElements(1) ) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(PCD_Gstack.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        Object tmp;
        tmp = gStack.pop();
        this.count -= 1;
        System.out.println("POP: " + (Integer)tmp + "  STACK COUNT: " + this.count);
        this.notifyAll();
        return tmp;
    }

    public int getMAX() {
        return this.MAXELEM;
    }

}