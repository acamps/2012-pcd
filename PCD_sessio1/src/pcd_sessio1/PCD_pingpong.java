/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_sessio1;

/**
 *
 * @author albertcamps
 */
public class PCD_pingpong extends Thread {
    private String word;
    private int delay;

    public PCD_pingpong(String whatToSay, int delayTime) {
        this.word = whatToSay;
        this.delay = delayTime;
    }
    
    public void run() {
        try {
            for (int i = 0; i < 30; i++) {
                System.out.println(this.word);
                sleep(this.delay);
            }
        } catch (InterruptedException e) {
            System.out.println("INTERRUMPTED");
            return;
        }
    }
    
    
}
