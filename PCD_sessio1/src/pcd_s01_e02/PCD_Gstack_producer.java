package pcd_s01_e02;

import pcd_s01_e01.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack_producer extends Thread{
    private PCD_Gstack pila;
    private String typeElems;

    public PCD_Gstack_producer(PCD_Gstack pila,String typeElems) {
        this.pila = pila;
        this.typeElems = typeElems;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            this.pila.PCD_Gstack_push((Object)i,this.typeElems);
            try {
                sleep(System.currentTimeMillis()%1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(PCD_Gstack_producer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
