/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_sessio1;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack_program {
    public static void main(String[] args) {
        PCD_Gstack h = new PCD_Gstack(200);
        PCD_Gstack_producer p = new PCD_Gstack_producer(h);
        PCD_Gstack_consumer c = new PCD_Gstack_consumer(h);
        
        p.start();
        c.start();
    }
}