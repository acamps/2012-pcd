/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_s01_e01;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack_consumer extends Thread {
    private PCD_Gstack pila;
    private int numElems;

    public PCD_Gstack_consumer(PCD_Gstack pila, int numElems) {
        this.pila = pila;
        this.numElems  = numElems;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            this.pila.PCD_Gstack_pop(this.numElems);
        }
    }
}
