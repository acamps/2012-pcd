/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_sessio0;

/**
 *
 * @author albertcamps
 */
public abstract class BST<T> {
    private BST<T> rightNode;
    private BST<T> leftNode;
    private T value;
    
    public void insert(BST<T> child, BST<T> tree) {
        
        int comp = compare(child.getValue(),tree.getValue());
        
        if(comp >=0) {
            if(tree.getRightNode() != null) {
                insert(child,tree.getRightNode());
            } else {
                tree.setRightNode(child);
            }
        } else {
            if(tree.getLeftNode() != null) {
                insert(child,tree.getLeftNode());
            } else {
                tree.setLeftNode(child);
            }
        }
    }
    
    public T getValue() {
        return value;
    }
    
    public void setValue(T value) {
        this.value = value;
    }

    public BST<T> getRightNode() {
        return rightNode;
    }

    public void setRightNode(BST<T> rightNode) {
        this.rightNode = rightNode;
    }

    public BST<T> getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(BST<T> leftNode) {
        this.leftNode = leftNode;
    }
    
    
    
    
    /**
     *
     */
    public void printInInorder(BST<T> element) {
    
        if(element.getLeftNode()!=null) {
            printInInorder(element.getLeftNode());
        }
        System.out.println(print(element));
        if(element.getRightNode()!=null) {
            printInInorder(element.getRightNode());
        }
    }
    
    public abstract String print(BST<T> a);
    public abstract int compare(T a, T b);
}

/**public class BST<Integer> implements BST<T> {}*/
