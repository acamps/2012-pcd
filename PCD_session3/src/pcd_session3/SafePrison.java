
public class SafePrison {
	
    public static void main(String[] args){

        //=== constructors
        Room room = new Room("on");
        int numPris = 49;
        Alice alice = new Alice(room, numPris);
        Prisoner[] prisoners = new Prisoner[numPris];
        
        for (int i = 0; i < numPris; i++) {
            prisoners[i] = new Prisoner(room, alice, i+1);
        }

        //==== start threads 
        alice.start();
        for (int i = 0; i < numPris; i++) {
            prisoners[i].start();
        }        
    }
	
}
