/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_sessio1;

/**
 *
 * @author albertcamps
 */
public class PCD_ValorEnter {
    private int enter;
    private Object lock;
    
    public synchronized void setEnter(int val) {
/*        synchronized (lock) { //Si alguien pide el lock si lo estoy utilizando se tiene que esperar.
            this.enter = val; 
        }*/
        this.enter = val;
    }
    
    public synchronized int getEnter() {
        return this.enter;
    }
}
