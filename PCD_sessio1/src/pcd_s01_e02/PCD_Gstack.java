/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_s01_e02;

import pcd_s01_e01.*;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack {
    private Stack gStack;
    private int MAXELEM;
    private int count;

    public PCD_Gstack(int MAXELEM) {
        this.gStack = new Stack();
        this.MAXELEM = MAXELEM;
        this.count = 0;
    }
    
    public int getCount() {
        return this.count;
    }
    
    private boolean reachedMax(int amount) {
        return ((this.count + amount) > this.MAXELEM);
    }
    
    private boolean enoughElements(int amount) {
        return(this.count > amount);
    }
    
    public synchronized void PCD_Gstack_push(Object object, String type) {
        
        while(reachedMax(1)) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(PCD_Gstack.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Object tmp = this.gStack.push(type);
        
        this.count += 1;
        this.notifyAll();
        System.out.println("PUSH: " + type + "  STACK COUNT: " + this.count);
    }
    
    public synchronized Object PCD_Gstack_pop(String type) {
        while(!enoughElements(1) || !isKind(type)) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(PCD_Gstack.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        Object tmp;
        tmp = gStack.pop();
        this.count -= 1;
        System.out.println("POP: " + tmp + "  STACK COUNT: " + this.count);
        this.notifyAll();
        return tmp;
    }

    public int getMAX() {
        return this.MAXELEM;
    }

    private boolean isKind(String type) {
        if(this.gStack.empty()) {
            return false;
        }
        return (String)this.gStack.peek() == type;
    }
}