/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_s01_e01;

import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack {
    private Stack gStack;
    private int MAXELEM;
    private int count;

    public PCD_Gstack(int MAXELEM) {
        this.gStack = new Stack();
        this.MAXELEM = MAXELEM;
        this.count = 0;
    }
    
    public int getCount() {
        return this.count;
    }
    
    private boolean reachedMax(int amount) {
        return ((this.count + amount) > this.MAXELEM);
    }
    
    private boolean enoughElements(int amount) {
        return(this.count > amount);
    }
    
    public synchronized void PCD_Gstack_push(Object object, int amount) {
        
        while(reachedMax(amount)) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(PCD_Gstack.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (int i = 0; i < amount; i++) {
            Object tmp = this.gStack.push(object);
        }
        
        this.count += amount;
        this.notifyAll();
        System.out.println("PUSH: " + (Integer)object + "  STACK COUNT: " + this.count);
    }
    
    public synchronized Object[] PCD_Gstack_pop(int amount) {
        while(!enoughElements(amount)) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(PCD_Gstack.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        Object[] tmpArray;
        tmpArray = new Object[amount];
        for (int i = 0; i < amount; i++) {
            tmpArray[i] = gStack.pop();
        }
        this.count -= amount;
        System.out.println("POP: " + (Integer)tmpArray[0] + "  STACK COUNT: " + this.count);
        this.notifyAll();
        return tmpArray;
    }

    public int getMAX() {
        return this.MAXELEM;
    }
}