package pcd_s01_e02;

import pcd_s01_e01.*;

/**
 * Producers produce different amount of elements, Consumers consume different amount of elements. Each time?
 * @author albertcamps
 */
public class PCD_Gstack_program {
    public static void main(String[] args) {
        PCD_Gstack h = new PCD_Gstack(20);
        
        PCD_Gstack_producer p0 = new PCD_Gstack_producer(h,"manzana");
        PCD_Gstack_producer p1 = new PCD_Gstack_producer(h,"pera");
        
        PCD_Gstack_consumer c0 = new PCD_Gstack_consumer(h,"manzana");
        PCD_Gstack_consumer c1 = new PCD_Gstack_consumer(h,"pera");
        PCD_Gstack_consumer c2 = new PCD_Gstack_consumer(h,"pera");
        
        p0.start();
        p1.start();
        
        c0.start();
        c1.start();
        c2.start();
    }
}