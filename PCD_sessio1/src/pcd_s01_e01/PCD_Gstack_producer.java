package pcd_s01_e01;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack_producer extends Thread{
    private PCD_Gstack pila;
    private int numElems;

    public PCD_Gstack_producer(PCD_Gstack pila,int numElems) {
        this.pila = pila;
        this.numElems = numElems;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            this.pila.PCD_Gstack_push((Object)i,this.numElems);
        }
    }
}
