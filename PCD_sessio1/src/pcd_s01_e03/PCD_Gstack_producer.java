package pcd_s01_e03;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack_producer extends Thread{
    private PCD_Gstack pila;

    public PCD_Gstack_producer(PCD_Gstack pila) {
        this.pila = pila;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            this.pila.PCD_Gstack_push((Object)i);
            try {
                sleep(System.currentTimeMillis()%1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(PCD_Gstack_producer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
