/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_sessio1;

/**
 *
 * @author albertcamps
 */
public class PCD_LlegeixEnter extends Thread {
    private PCD_ValorEnter valor;
    
    public PCD_LlegeixEnter(PCD_ValorEnter valorInicial) {
        this.valor = valorInicial;
    }
    
    public void run() {
        for (int i = 0; i < 20; i++) {
            this.valor.getEnter();
            System.out.println("Lector escriu : " + i);
            
            try {
                sleep ( (int) Math.random() * 3000);
            } catch (InterruptedException e) {
                System.err.println("Exception " + e.toString());
            }
        }
    }
    
}
