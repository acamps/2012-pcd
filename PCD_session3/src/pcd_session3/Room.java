
public class Room {

	private String light;
	
	public Room(String light){		
		this.light = light;
	}
	
	public String getState(){
		return light;
	}
	
	public String turnOn(){
		if(light == "off") {
			light ="on";
			return "Light is: on"; 
		}
		else return "You cannot turnOn a light on";
	}
	
	public String turnOff(){
		if(light == "on") {
			light ="off";
			return "Light is: off"; 
		}
		else return "You cannot turnOff a light off";
	}
	
	
	
	public String toString(){
		return "Light is: " + light;
	}

}
