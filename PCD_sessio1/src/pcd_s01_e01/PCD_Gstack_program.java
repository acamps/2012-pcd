package pcd_s01_e01;

/**
 * Producers produce different amount of elements, Consumers consume different amount of elements. Each time?
 * @author albertcamps
 */
public class PCD_Gstack_program {
    public static void main(String[] args) {
        PCD_Gstack h = new PCD_Gstack(20);
        PCD_Gstack_producer p = new PCD_Gstack_producer(h,3);
        PCD_Gstack_consumer c = new PCD_Gstack_consumer(h,4);
        
        p.start();
        c.start();
    }
}