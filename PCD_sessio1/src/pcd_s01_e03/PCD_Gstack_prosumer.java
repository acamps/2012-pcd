/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_s01_e03;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack_prosumer extends Thread {
    private PCD_Gstack pila;
    private int numProd;

    
    public PCD_Gstack_prosumer(PCD_Gstack pila, int numProd) {
        this.pila = pila;
        this.numProd = numProd;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            if(this.pila.enoughElements(1)) {
                this.pila.PCD_Gstack_pop();
            } else {
                for (int j = 0; j < this.numProd; j++) {
                    this.pila.PCD_Gstack_push(i);
                }
            }
        }
    }
}
