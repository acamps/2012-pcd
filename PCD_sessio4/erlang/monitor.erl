-module(monitor).
-export([init_monitor/3, global_monitoring/3, create_tasks/4, new_task/3,init_device/3]).

init_monitor(N,M,NumberOfTasks) ->
  if 
    N*M >= NumberOfTasks ->
      exit(invalidNumberOfTasks);
    true ->
      PidMonitor = spawn(monitor, global_monitoring,[NumberOfTasks,0,0]),
      create_tasks(N,M,M,PidMonitor)
  end.

create_tasks(ROW, COL, MAX_COL, PidMonitor) ->
	io:format("CREATE TASKS IN~n"),
	if
  		ROW > 0 ->
  			new_task(ROW, COL, PidMonitor),
  			if
  				COL - 1 > 0 ->
  					create_tasks(ROW, COL-1, MAX_COL, PidMonitor);
  				COL - 1 =< 0 ->
  					create_tasks(ROW-1, COL, MAX_COL, PidMonitor)
  			end;
  		ROW =< 0 ->
  			exit(finished)
	end.

new_task(ROW, COL, PidMonitor) ->
	io:format("created task~n"),
	spawn(monitor, init_device, [ROW,COL, PidMonitor]).

init_device(ROW, COL, PidMonitor) ->
	R = random:uniform(COL),
	if
		(R rem 2) =:= 0 ->
			PidMonitor ! {fail,ROW,COL},
			exit(failed);
		(R rem 2) =/= 0 ->
		PidMonitor ! {finish, ROW, COL, self()}
	end,

	receive
		restart ->
			if
				R rem 2 =:= 0 ->
					PidMonitor ! {finish, ROW, COL, self()};
				R rem 2 =/= 0 ->
					PidMonitor ! {fail, ROW, COL}
			end
		end.

global_monitoring(NumberOfRemaining, NumberOfFailures, NumberOfFinished) ->
	if
		NumberOfRemaining =/= 0 ->
			io:format("Number of Failures: ~p~n",[NumberOfFailures]),
			io:format("Number of Finished: ~p~n",[NumberOfFinished]),
			io:format("Number of Remaining: ~p~n",[NumberOfRemaining]),
			receive
				{fail, ROW, COL} ->
					io:format("The task (~p,~p) has failed. ~n",[ROW, COL]),
					new_task(ROW, COL, self()),
					global_monitoring(NumberOfRemaining-1, NumberOfFailures+1, NumberOfFinished);
				{finish, ROW, COL, Pid} ->
					io:format("The task (~p,~p) has finished. ~n",[ROW, COL]),
					Pid ! restart,
					global_monitoring(NumberOfRemaining-1, NumberOfFailures, NumberOfFinished+1)
				end;
		NumberOfRemaining =:= 0 ->
			io:format("Number of Failures: ~p~n",[NumberOfFailures]),
			io:format("Number of Finished: ~p~n",[NumberOfFinished]),
			io:format("Number of Remaining: ~p~n",[NumberOfRemaining]),
			exit(finishedNumberOfTasks)
	end.