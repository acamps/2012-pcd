/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_s01_e03;

/**
 *
 * @author albertcamps
 */
public class PCD_Gstack_consumer extends Thread {
    private PCD_Gstack pila;

    public PCD_Gstack_consumer(PCD_Gstack pila) {
        this.pila = pila;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            this.pila.PCD_Gstack_pop();
        }
    }
}
