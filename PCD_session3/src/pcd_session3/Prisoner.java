
public class Prisoner extends Thread{
	Room room;
	boolean firstTime=true;
        boolean secondTime=true;
	boolean free;
	Alice myAlice;
	// this is the prisoner number
	int i;
	
	public Prisoner(Room room, Alice alice, int i){
		this.room=room;
		myAlice =alice;
		this.i = i;
	}
	
	//=== entering the room====
	
	public void roomIn(){
		synchronized(room){
			if(firstTime){
				if(room.getState()=="off"){
					room.turnOn();
                                        if(secondTime) {
                                            secondTime=false;
                                        }else {
                                            firstTime=false;
                                        }
					System.out.println("Prisoner " + i +" turnOn the light for the first time");
				} else{
					room.turnOff();room.turnOn();
					System.out.println("Prisoner " + i + " turnOff->turnOn the light before the first time");
				}
			}
                        else if(!firstTime){
				if(room.getState()=="off"){
					room.turnOn();room.turnOff();
					System.out.println("Prisoner " + i + " turnOn->turnOff the light after the first time");
				} else{
				room.turnOff();room.turnOn();
				System.out.println("Prisoner " + i + " turnOff->turnOn the light after the first time");
				}
			}
			free = myAlice.askForFree();
			System.out.println("Prisoner " + i + " ask Alice if free: " + free);
			//Simulate.HWInterrup();
		}
		Simulate.HWInterrup();
	}
	
	//======= the run ===========
	
	public void run(){
		// for(int i=1; i<100&& !free; ++i)roomIn();
		
		while(!free){roomIn();}
	}
	
}
