package pcd_s01_e03;

/**
 * Producers produce different amount of elements, Consumers consume different amount of elements. Each time?
 * @author albertcamps
 */
public class PCD_Gstack_program {
    public static void main(String[] args) {
        PCD_Gstack h = new PCD_Gstack(20);
        
        PCD_Gstack_producer p0 = new PCD_Gstack_producer(h);
        PCD_Gstack_producer p1 = new PCD_Gstack_producer(h);
        
        PCD_Gstack_consumer c0 = new PCD_Gstack_consumer(h);
        PCD_Gstack_consumer c1 = new PCD_Gstack_consumer(h);
        PCD_Gstack_consumer c2 = new PCD_Gstack_consumer(h);
        
        PCD_Gstack_prosumer ps0 = new PCD_Gstack_prosumer(h,3);
        
        
        ps0.start();
        p0.start();
        p1.start();
        
        c0.start();
        c1.start();
        c2.start();
    }
}