/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pcd_sessio0;


class BST_int extends BST<Integer> {

    public BST_int(Integer a) {
        this.setValue(a);
        this.setLeftNode(null);
        this.setRightNode(null);
    }


    @Override
    public int compare(Integer a, Integer b) {
        if(a>b) {
            return 1;
        }
        else if(a == b) {
            return 0;
        }
        else {
            return -1;
        }
    }

    @Override
    public String print(BST<Integer> a) {
        return String.valueOf(a.getValue());
    }
}
/**
 * abstract class X<T> {
    public abstract T yourMethod();
}
class X1 extends X<X1> {
    public X1 yourMethod() {
        return this;
    }
}
class X2 extends X<X2> {
    public X2 yourMethod() {
        return this;
    }
}
 */